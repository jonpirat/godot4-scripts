# Editor-like camera movement
# Panning: shift + middle mouse
# Zoom: mouse wheel
# Rotate view: middle mouse

extends Camera3D

@onready var project_window_resolution = Vector2(ProjectSettings.get_setting("display/window/size/viewport_width"), ProjectSettings.get_setting("display/window/size/viewport_height"))
@onready var screen_size = get_viewport().size

@export var pan_speed = 0.6
@export var rotate_speed = 0.5
@export var zoom_speed = 5

const BUTTON_WHEEL_UP = 4
const BUTTON_WHEEL_DOWN = 5
const MIDDLE_WHEEL = 3

var start_drag = Vector2()
var dragging = false
var shift = false
var screen_size_x_mod = 1
var screen_size_y_mod = 1
var zoom = 0


func _process(delta):
	screen_size = get_viewport().size
	screen_size_x_mod = project_window_resolution.x / screen_size.x
	screen_size_y_mod = project_window_resolution.y / screen_size.y

	if dragging and shift:
		var offset = start_drag - get_viewport().get_mouse_position()
		offset = Vector3(offset.x * screen_size_x_mod * pan_speed * delta, offset.y * screen_size_y_mod * pan_speed * delta * -1, 0)
		translate_object_local(offset)
		start_drag = get_viewport().get_mouse_position()
	elif dragging:
		var offset = start_drag - get_viewport().get_mouse_position()
		offset = Vector3(offset.x * screen_size_x_mod * rotate_speed * delta, offset.y * screen_size_y_mod * rotate_speed * delta * -1, 0)
		var rot = get_global_rotation()
		rot.y += offset.x
		rot.x -= offset.y
		set_global_rotation(rot)
		start_drag = get_viewport().get_mouse_position()
	
	if zoom != 0:
		var zoom_offset = Vector3(0, 0, zoom * delta)
		translate_object_local(zoom_offset)
		zoom = 0
		

func _input(event):
	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_SHIFT:
			shift = true
		elif event.keycode == KEY_SHIFT and event.pressed == false:
			shift = false
			
	if event is InputEventMouseButton and event.pressed and event.button_index == MIDDLE_WHEEL:
		dragging = true
		start_drag = event.position
	elif event is InputEventMouseButton and event.button_index == BUTTON_WHEEL_UP:
		dragging = false
		zoom -= zoom_speed
	elif event is InputEventMouseButton and event.button_index == BUTTON_WHEEL_DOWN:
		dragging = false
		zoom += zoom_speed
	elif event is InputEventMouseButton:
		dragging = false

