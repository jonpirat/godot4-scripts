# a camera controller that is made to feel like the camera in typical top-down
# rpg games like bg3. Place the script on a node3d and parent the camera under the node3d.

extends Node3D

var middle_click = 3
var middle_zoom_in = 4
var middle_zoom_out = 5

var start_drag_mouse = Vector2()
var rotateSpeed = 0.05
var zoomSpeed = 0.75
var zoom = 0
var dragging = false
var rotateAxis = Vector3(0,1,0)
var moveSpeed = 0.05
var cam_move_vector = Vector3()

var max_zoom_in = 6
var max_zoom_out = 12

var default_fps = 60.0

@onready var cam = get_node("Camera3D")
@onready var camDistance = self.global_position.distance_to(cam.global_position)
@onready var project_window_resolution = Vector2(ProjectSettings.get_setting("display/window/size/viewport_width"), ProjectSettings.get_setting("display/window/size/viewport_height"))
@onready var screen_size = get_viewport().size

func _ready():
	pass 

func _process(delta):
	var dt = (1.0 / default_fps) / delta
	
	screen_size = get_viewport().size
	var screen_size_x_mod = project_window_resolution.x / screen_size.x
	camDistance = self.global_position.distance_to(cam.global_position)
	
	if dragging:
		# var mousePos = cam.project_position(get_viewport().get_mouse_position(), camDistance)
		var mousePos = get_viewport().get_mouse_position()
		var offset = (start_drag_mouse - mousePos)
		# print(offset.x)

		if abs(offset.x) > 0.02:
			var dirMod = 1
			if offset.x < 0:
				dirMod = -1
				
			var rot = get_global_rotation()
			rot.y += dirMod * rotateSpeed * dt
			set_global_rotation(rot)
			start_drag_mouse = mousePos
	
	if zoom !=0:
		if zoom > zoomSpeed:
			zoom = zoomSpeed
		elif zoom < -zoomSpeed:
			zoom = - zoomSpeed
		
		var zoom_offset = Vector3(0, 0, zoom * dt)
		cam.translate_object_local(zoom_offset)
		
		if zoom > 0:
			zoom -= zoomSpeed * 2 * dt
			if zoom < 0:
				zoom = 0
		elif zoom < 0:
			zoom += zoomSpeed * 2 * dt
			if zoom > 0:
				zoom = 0
	
	var cam_move_input = false
	if Input.is_action_pressed("camera_forward"):
		if cam_move_vector.x > -moveSpeed:
			cam_move_vector.x -= moveSpeed * dt
			cam_move_input = true
		
	if Input.is_action_pressed("camera_back"):
		if cam_move_vector.x < moveSpeed:
			cam_move_vector.x += moveSpeed * dt
			cam_move_input = true
		
	if Input.is_action_pressed("camera_left"):
		if cam_move_vector.z < moveSpeed:
			cam_move_vector.z += moveSpeed * dt
			cam_move_input = true
	
	if Input.is_action_pressed("camera_right"):
		if cam_move_vector.z > -moveSpeed:
			cam_move_vector.z -= moveSpeed * dt
			cam_move_input = true
		
	if cam_move_input == false:
		cam_move_vector = cam_move_vector.lerp(Vector3(0,0,0), moveSpeed * 1.5 * dt)
	
	translate_object_local(cam_move_vector)

func _input(event):
	# Mouse in viewport coordinates.
	if event is InputEventMouseButton:
		if event.is_released() and event.button_index == middle_click:
			dragging = false
			
		if event.is_pressed() and event.button_index == middle_click:
			dragging = true
			# start_drag_mouse = cam.project_position(event.position, camDistance)
			start_drag_mouse = event.position
			
		if event is InputEventMouseButton and event.button_index == middle_zoom_in:
			if camDistance > max_zoom_in:
				zoom -= zoomSpeed
		elif event is InputEventMouseButton and event.button_index == middle_zoom_out:
			if camDistance < max_zoom_out:
				zoom += zoomSpeed
	
