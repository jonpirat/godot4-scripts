# Godot4 Scripts

This repo is a place where I can save useful scripts for Godot 4. 
Each script has some comments in the header explaining how to use it. 

# Scripts
## Blender Cam
A scipt that can be attached to a Camera3D object. Let's you move the camera in-game i the same manner-ish as you move the camera in Blender. 

## Authors and acknowledgment
Written by me, Jon Aspeheim. 

## License
No license, go ahead and copy. 
